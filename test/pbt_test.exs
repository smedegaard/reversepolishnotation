defmodule PbtTest do
  use ExUnit.Case
  use PropCheck

  alias ReversePolishNotation, as: RPN

  # Properties
  property "can add number to list of numbers" do
    forall {list, number} <- {list(number()), number()} do
      {:ok, new_list} = RPN.push(list, number)
      number == List.first(new_list)
    end
  end

  property "can push operator to a list with minimum 2 numbers" do
    forall {list, operator} <- {[number() | list_of_numbers()], operator()} do
      {result, _new_list} = RPN.push(list, operator)
      result == :ok
    end
  end

  property "division by zero returns an error tuple" do
    forall list <- [number(), 0 | list(number())] do
      result = RPN.push(list, :/)
      {:error, list, :division_by_zero} == result
    end
  end

  property "non-division operators are applied on the two first elements" do
    forall {[x, y | _tail] = list, operator} <-
             {[number(), number() | number() |> list()], oneof([:+, :-, :*])} do
      result = apply(Kernel, operator, [x, y])
      {:ok, new_list} = RPN.push(list, operator)
      result == List.first(new_list)
    end
  end

  property "division is applied on the two first elements" do
    forall {[x, y | _tail] = list, operator} <-
             {[not_zero(), not_zero() | number() |> list()], :/} do
      result = apply(Kernel, operator, [x, y])
      {:ok, new_list} = RPN.push(list, operator)
      result == List.first(new_list)
    end
  end

  # Generators

  def operator() do
    oneof([:+, :-, :/, :*])
  end

  def not_zero() do
    such_that(n <- number(), when: n != 0)
  end

  def list_of_numbers() do
    not_zero()
    |> list()
    |> non_empty()
  end
end
