defmodule ReversePolishNotation do
  @moduledoc """
  Documentation for ReversePolishNotation.
  """

  def push(list, number) when number |> is_number do
    {:ok, [number | list]}
  end

  def push([_x, 0 | _tail] = list, :/) do
    {:error, list, :division_by_zero}
  end

  @operators [:+, :/, :-, :*]
  def push([x, y | list], op) when op in @operators do
    result = apply(Kernel, op, [x, y])
    {:ok, [result | list]}
  end

  def push(list, input) do
    {:error, list, {:unexpected_input, input}}
  end
end
